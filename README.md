# FSW5_INHERITANCE_04_03

TUGAS KELOMPOK 1- INHERITANCE IN JAVASCRIPT OOP

## Langkah-langkah

Untuk mengunduh repository ini ke dalam komputer lokal:

```
$ git clone https://gitlab.com/cherdianto/fsw5_inheritance_04_03
```

Untuk membuat branch baru:

```
$ git checkout -b [nama branch / nama fitur]
```

Jangan lupa untuk pindah ke branch masing-masing:

```
$ git checkout [nama branch / nama fitur]
```

Unggah setiap pekerjaan ke masing-masing branch:

```
$ git push origin [nama branch / nama fitur]
```

Satukan dengan branch development (develop):

```
$ git checkout develop
$ git merge [nama branch / nama fitur]
```
